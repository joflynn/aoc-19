const day = process.argv[2];
const part = process.argv[3] || "one";

const main = require(`./src/${process.argv[2]}`);

const { one, two } = main();

if ( part === "one" ) {
  console.log(one());
  return;
}
console.log(two());
