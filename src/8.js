const { parse } = require("./utils");

module.exports = () => {
  const [input] = parse(8);

  //.map(x => x.split("").map(y => parseInt(y)));

  const cols = 25;
  const rows = 6;

  const layers = [];
  let i = 0;
  while (i < input.length) {
    layers.push(
      input
        .slice(i, i + rows * cols)
        .split("")
        .map(x => parseInt(x))
    );
    i += rows * cols;
  }

  function one() {
    const zeroCounts = layers.map(layer => {
      return layer.reduce((acc, cur) => (cur === 0 ? acc + 1 : acc), 0);
    });
    const fewest = Math.min(...zeroCounts);
    const fewestIdx = zeroCounts.findIndex(x => x === fewest);
    console.log(fewestIdx);

    console.log(layers[fewestIdx]);
    const layer = layers[fewestIdx];
    const ones = layer.reduce((acc, cur) => (cur === 1 ? acc + 1 : acc), 0);
    const twos = layer.reduce((acc, cur) => (cur === 2 ? acc + 1 : acc), 0);
    console.log(ones, twos);
    return ones * twos;
  }

  const TRANSPARENT = "-";
  const BLACK = "█";
  const WHITE = " ";
  const ENUM = [BLACK, WHITE, TRANSPARENT];
  function two() {
    layers.reverse();
    const image = layers.shift();
    layers.map(layer => {
      layer.forEach((pixel, idx) => {
        if (pixel !== 2) {
          image[idx] = pixel;
        }
      });
      //cat(image);
    });

    cat(image);
  }

  function cat(layer) {
    let i = 0;
    for (let i = 0; i < rows; i += 1) {
      const begin = i * cols;
      const end = begin + cols;
      line = layer.slice(begin, end);
      console.log(line.map(x => ENUM[x]).join(""));
    }
    console.log();
  }

  return Object.freeze({ one, two });
};
