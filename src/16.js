const { parse } = require("./utils");
module.exports = () => {
  const input = parse(16, "").map(x => parseInt(x));

  const SIN = [0, 1, 0, -1];

  function pattern({ length, index }) {
    const p = [];
    const multiplier = index + 1;
    const stop = length + 1;
    for (let i = 0; i < stop; i++) {
      const sinIdx = Math.floor(i / multiplier);
      p.push(SIN[sinIdx % SIN.length]);
    }
    p.shift();
    return p;
  }

  function fft(input) {
    return input.map((_, idx) => {
      const p = pattern({ length: input.length, index: idx });
      const series = input.map((digit, i) => digit * p[i]);
      return Math.abs(series.reduce((a, c) => a + c, 0)) % 10;
    });
  }

  function one() {
    let test = [1, 2, 3, 4, 5, 6, 7, 8];
    test = "80871224585914546619083218645595"
      .split("")
      .map(x => parseInt(x));
    //const input = test;
    console.log(input);
    console.log(input.length);

    const length = input.length;
    let value = input;
    for (let i = 1; i <= 100; i += 1) {
      console.log(value);
      value = fft(value);
    }
    console.log(value.slice(0, 8));
    return value.slice(0, 8).join("");
  }

  function two() {
    console.log(input.length);
    const repeated = input
      .join("")
      .repeat(10000)
      .split("")
      .map(x => parseInt(x));

    const pos = parseInt(input.slice(0, 7).join(""));
    console.log(pos);
    console.log(repeated.length);
    let value = repeated.slice(pos);
    console.log(value.length);

    for (let i = 0; i < 100; i += 1) {
      let count = 0;
      //work backwards
      let next = new Array(value.length);
      for (
        let location = value.length - 1;
        location >= 0;
        location -= 1
      ) {
        count = (count + value[location]) % 10;
        next[location] = count;
      }
      value = next;
      console.log(value.slice(0, 80).join(""));
    }

    return value.slice(0, 8).join("");
  }

  return Object.freeze({ one, two });
};
