const { parse, sum } = require("./utils");

module.exports = () => {

  const data = parse(1).map(x => parseInt(x));

  function fuelFor(mass) {
    const fuel = Math.floor(mass / 3) - 2;
    if (fuel < 0){
      return 0;
    }
    return fuel;
  }

  function one() {
    return sum(data.map(fuelFor));
  }

  function two() {
    return sum(data.map(moduleMass => {
      console.log("---");
      console.log("module Mass:", moduleMass);
      let totalModuleWeight = 0;
      let cur = fuelFor(moduleMass);
      while (cur > 0) {
        totalModuleWeight += cur;
        cur = fuelFor(cur);
      }
      return totalModuleWeight;
    }));
  }

  return Object.freeze({one, two});
}
