const { parse, sum } = require("./utils");
const Computer = require("./computer");

module.exports = () => {
  const data = parse(5, ",").map(x => parseInt(x));

  const inputs = [];
  const outputs = [];

  function one() {
    inputs.push(1);
    console.log(inputs);
    const computer = Computer({ data, inputs, outputs });
    computer.execute(true);
  }

  function two() {
    inputs.push(5);
    const computer = Computer({ data, inputs, outputs });
    const debug = true;
    computer.execute(debug);
  }

  return Object.freeze({ one, two });
};
