const { parse } = require("./utils");
const RadarQueue = require("./radarQueue");
module.exports = () => {
  const input = parse(10).map(x => x.split(""));

  const ASTEROID = "#";

  function show(grid) {
    grid.forEach(line => console.log(line.join("")));
  }

  function toLine({ from, to }) {
    if (from.x === to.x && from.y === to.y) {
      return;
    }
    const m = (to.y - from.y) / (to.x - from.x);
    const b = from.y - m * from.x;
    return { m, b };
  }
  function toBoundary({ from, theta, bounds }) {
    let r = 0;
    const dr = 0.01;
    const pos = { x: from.x, y: from.y };
    while (inBounds({ pos, bounds })) {
      r += dr;
      pos.x = Math.sin(theta) * r;
      pos.y = Math.cos(theta) * r;
    }
    console.log(pos, nearest(pos));
    return nearest({ pos });
  }
  function makeBoundaryList({ domain, range, home }) {
    const list = [];
    for (let x = home.x; x < domain[1]; x += 1) {
      list.push({ x, y: range[0] });
    }
    for (let y = range[0]; y < range[1]; y += 1) {
      list.push({ x: domain[1], y });
    }
    for (let x = domain[1]; x > domain[0]; x -= 1) {
      list.push({ x, y: range[1] });
    }
    for (let y = range[1]; y < range[0]; y -= 1) {
      list.push({ x: domain[0], y });
    }
    for (let x = domain[0]; x < home.x; x += 1) {
      list.push({ x, y: range[0] });
    }

    return list;
  }

  function inBounds({ pos, bounds }) {
    const { domain, range } = bounds;
    return (
      domain[0] < pos.x &&
      pos.x < domain[1] &&
      range[0] < pos.y &&
      pos.y < range[1]
    );
  }

  function nearest({ x, y }) {
    return {
      x: Math.round(x),
      y: Math.round(y)
    };
  }
  function outside({ line, from, to, point }) {
    if (line.m === Infinity || line.m === -Infinity) {
      return (
        (point.y < from.y && point.y < to.y) ||
        (point.y > from.y && point.y > to.y)
      );
    }
    return (
      (point.x < from.x && point.x < to.x) ||
      (point.x > from.x && point.x > to.x)
    );
  }
  function colinearAsteroids({ from, to, line, asteroids }) {
    return asteroids
      .filter(p => !(p.x === to.x && p.y === to.y))
      .map(other => ({
        ...other,
        ...toLine({ from, to: other })
      }))
      .filter(x => x)
      .filter(l => {
        return l.m === line.m && l.b === line.b;
      });
  }
  function correctDirection({ from, vector, target }) {
    const dxVectorPos = vector.x - from.x > 0;
    const dyVectorPos = vector.y - from.y > 0;
    const dxTargetPos = target.x - from.x > 0;
    const dyTargetPos = target.y - from.y > 0;

    return dxVectorPos === dxTargetPos && dyVectorPos === dyTargetPos;
  }

  function lineOfSight({ from, to, asteroids }) {
    line = toLine({ from, to });
    if (!line) {
      return false;
    }
    const colinear = colinearAsteroids({ from, to, line, asteroids });
    if (colinear.length === 0) {
      return true;
    }
    return colinear.every(point =>
      outside({ line, to, from, point })
    );
  }

  function scan({ from, asteroids }) {
    let count = 0;
    let stats = asteroids.map(to =>
      lineOfSight({ from, to, asteroids })
    );
    count = stats.filter(x => x).length;
    return count;
  }

  function asteroidPositions(input) {
    const asteroids = [];
    input.forEach((row, y) => {
      row.forEach((cell, x) => {
        if (cell === ASTEROID) {
          asteroids.push({ x, y });
        }
      });
    });
    return asteroids;
  }

  function one() {
    const asteroids = asteroidPositions(input);
    show(input);
    let max = 0;
    let maxPos;
    asteroids.forEach(({ x, y }) => {
      const count = scan({
        from: { x, y },
        grid: input,
        asteroids
      });
      input[y][x] = count;
      if (count >= max) {
        console.log("new max", maxPos, max, "=>", { x, y }, count);
        max = count;
        maxPos = { x, y };
      }
    });

    show(input);
    return max;
  }

  const EPSILON = 0.01;
  function two() {
    //     const input = `.#....#####...#..
    // ##...##.#####..##
    // ##...#...#.#####.
    // ..#.....X...###..
    // ..#.#.....#....##`
    //       .split("\n")
    //       .map(x => x.split(""));
    //     const home = { x: 8, y: 3 };
    const home = { x: 28, y: 29 };
    input[home.y][home.x] = "X";
    show(input);

    let asteroids = asteroidPositions(input);
    const domain = [0, Math.max(...input.map(line => line.length))];
    const range = [0, input.length];
    const queue = RadarQueue({
      domain,
      range,
      home,
      init: -Math.PI / 2
    });

    let n = 1;
    let x, y;
    let theta = queue.next();
    console.log(theta);
    while (n <= 200) {
      const targets = queue.cur();

      const target = targets.find(({ x, y }) =>
        asteroids.find(tgt => tgt.x === x && tgt.y === y)
      );
      if (target) {
        x = target.x;
        y = target.y;
        console.log("fire", target);
        input[target.y][target.x] = n % 10;
        n += 1;
        if (n % 10 === 0) {
          show(input);
        }

        //remove
        asteroids = asteroids.filter(
          a => !(a.x === target.x && a.y === target.y)
        );
      }
      theta = queue.next();
    }
    return x * 100 + y;
  }

  return Object.freeze({ one, two });
};
