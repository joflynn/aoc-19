const { parse } = require("./utils");
module.exports = () => {
  const input = parse().map(x => x.split(","));

  function one() {}

  function two() {}
  return Object.freeze({ one, two });
};
