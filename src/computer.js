const { sleep } = require("./utils");
module.exports = ({ data, inputs, outputs }) => {
  let halted = false;

  function get({ mode, position }) {
    if (mode) {
      return data[position];
    }
    return data[data[position]];
  }
  function set(position, value) {
    data[data[position]] = value;
  }

  const OP_ADD = 1;
  const OP_MULT = 2;
  const OP_INPUT = 3;
  const OP_TEST = 4;
  const OP_JIT = 5;
  const OP_JIF = 6;
  const OP_LT = 7;
  const OP_EQ = 8;
  const OP_HALT = 99;

  const opcodes = {
    [OP_ADD]: (a, b, { position }) => {
      //console.log(`add save to ${data[position].toString(16)}`);
      //data[data[position]] = get(a) + get(b);
      set(position, get(a) + get(b));
    },
    [OP_MULT]: (a, b, { position }) => {
      //console.log(`multiply save to ${data[position].toString(16)}`);
      //data[data[position]] = get(a) * get(b);
      set(position, get(a) * get(b));
    },
    [OP_INPUT]: ({ position }) => {
      //console.log(`write to ${data[position].toString(16)}`);
      //data[data[position]] = inputs.shift();
      set(position, inputs.shift());
    },
    [OP_TEST]: a => {
      //console.log("".padEnd(80, "-") + "TEST".padEnd(80, "-"));
      const output = get(a);
      outputs.push(output);
      if (output) {
        throw output;
      }
    },
    [OP_JIT]: (a, b) => {
      const test = get(a);
      if (test !== 0) {
        pc = get(b);
        return true;
      }
    },
    [OP_JIF]: (a, b) => {
      const test = get(a);
      if (test === 0) {
        pc = get(b);
        return true;
      }
    },
    [OP_LT]: (a, b, { position }) => {
      set(position, get(a) < get(b) ? 1 : 0);
    },
    [OP_EQ]: (a, b, { position }) => {
      set(position, get(a) === get(b) ? 1 : 0);
    }
  };
  const arities = {
    [OP_ADD]: 3,
    [OP_MULT]: 3,
    [OP_INPUT]: 1,
    [OP_TEST]: 1,
    [OP_JIT]: 2,
    [OP_JIF]: 2,
    [OP_LT]: 3,
    [OP_EQ]: 3
  };

  function arity(opcode) {
    return arities[opcode];
  }

  function parseOpcode(opcode) {
    let op,
      modes = [];
    if (opcode.length > 2) {
      op = parseInt(opcode.slice(opcode.length - 2));
      modes = opcode
        .slice(0, opcode.length - 2)
        .split("")
        .map(x => parseInt(x))
        .reverse();
    } else {
      op = parseInt(opcode);
      modes = [];
    }

    while (modes.length < arity(op)) {
      modes.push(0);
    }

    return { op, modes };
  }

  let pc = 0;

  function cat() {
    let x = 0;
    const headers = Array.from({ length: 16 }, (_, i) => i);
    console.log(...["0x", ...headers].map(v => v.toString(16).padStart(8)));
    while (x < data.length) {
      console.log(
        ...[
          (x / 16).toString(16) + "0:",
          data[x],
          data[x + 1],
          data[x + 2],
          data[x + 3],
          data[x + 4],
          data[x + 5],
          data[x + 6],
          data[x + 7],
          data[x + 8],
          data[x + 9],
          data[x + 10],
          data[x + 11],
          data[x + 12],
          data[x + 13],
          data[x + 14],
          data[x + 15]
        ].map(v => v !== undefined && v.toString().padStart(8))
      );
      x += 16;
    }
  }
  function execute(debug = false) {
    while (pc < data.length) {
      const { op, modes } = parseOpcode(data[pc].toString());

      const args = modes.map((mode, idx) => ({ mode, position: pc + idx + 1 }));
      if (op === OP_HALT) {
        return data[0];
      }

      if (debug) {
        console.log({ pc: pc.toString(16), op, args });
      }

      let skip = false;
      try {
        skip = opcodes[op].apply(null, args);
      } catch (diagnostic) {
        pc += arity(op) + 1;
        const next = data[pc];
        if (next === OP_HALT) {
          halted = true;
        }
        return diagnostic;
      }

      if (debug) {
        console.log("---");
        cat();
        console.log("---");
        //await sleep(1000 * 1);
      }

      if (!skip) {
        pc += arity(op) + 1;
      }
    }
  }

  function run(newInput) {
    inputs.push(newInput);
    console.log(inputs);
    return execute();
  }
  function isHalted() {
    return halted;
  }

  return Object.freeze({ cat, execute, run, isHalted });
};
