module.exports = function RadarQueue({ domain, range, home, init }) {
  let thetas = [];
  let queue = {}; //indexed by theta

  for (let x = domain[0]; x < domain[1]; x += 1) {
    for (let y = range[0]; y < range[1]; y += 1) {
      const dx = x - home.x;
      const dy = y - home.y;

      const r = Math.sqrt(dx * dx + dy * dy);
      //const theta = Math.arcCos(dy / r) / Math.arcSin(dx / r);
      const theta = Math.atan2(dy, dx);

      if (r > 0) {
        if (!thetas.includes(theta)) {
          thetas.push(theta);
        }
        if (queue[theta] === undefined) {
          queue[theta] = [];
        }
        queue[theta].push({ x, y, r, theta });
      }
    }
  }
  thetas = thetas.sort((a, b) => a - b);

  let thetaIdx = 0;
  if (init !== undefined) {
    while (thetas[thetaIdx] < init) {
      next();
    }
    prev();
  }
  function cur() {
    const theta = thetas[thetaIdx];
    console.log(theta);
    return queue[theta].sort((a, b) => a.r - b.r);
  }
  function next() {
    console.log(thetaIdx, thetas[thetaIdx]);
    thetaIdx = (thetaIdx + 1) % thetas.length;
    console.log(thetaIdx, thetas[thetaIdx]);
    return thetas[thetaIdx];
  }
  function prev() {
    thetaIdx = (thetaIdx + thetas.length - 1) % thetas.length;
  }

  console.log(thetas, thetas.length);
  return Object.freeze({ cur, next });
};
