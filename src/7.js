const { parse, permutations } = require("./utils");
const Computer = require("./computer");

module.exports = () => {
  const data = parse(7, ",").map(x => parseInt(x));

  const inputs = [];
  const outputs = [];

  function serial(sequence) {
    return sequence.reduce((acc, cur) => {
      const mem = data.slice();
      const outputs = [];
      const computer = Computer({ data: mem, inputs: [cur, acc], outputs });
      return computer.execute();
    }, 0);
  }

  function feedback(sequence) {
    let pc = 0;
    let cur = 0;
    const amps = sequence.map(init => {
      const mem = data.slice();
      return Computer({ data: mem, inputs: [init], outputs: [] });
    });
    while (!amps[pc % amps.length].isHalted()) {
      cur = amps[pc % amps.length].run(cur);
      pc += 1;
    }
    return cur;
  }

  function one() {
    const perms = permutations([0, 1, 2, 3, 4]);
    const results = [];
    perms.forEach((sequence, idx) => {
      const result = serial(sequence);
      console.log("----", result);
      results[idx] = result;
    });
    const max = Math.max(...results);
    console.log(perms[results.findIndex(x => x === max)]);
    return max;
  }

  function two() {
    const perms = permutations([5, 6, 7, 8, 9]);
    const results = [];
    //const idx = 0;
    //const sequence = perms[idx];
    perms.forEach((sequence, idx) => {
      const result = feedback(sequence);
      console.log("----", result);
      results[idx] = result;
    });
    const max = Math.max(...results);
    console.log(perms[results.findIndex(x => x === max)]);
    return max;
  }

  return Object.freeze({ one, two });
};
