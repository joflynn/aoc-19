const { parse, sum } = require("./utils");
const Computer = require("./computer");

module.exports = () => {

  const input = parse(2, ",").map(x => parseInt(x));





  function one(noun = 12, verb = 2, debug = true) {
    const data = input.slice(0);
    data[1] = noun;
    data[2] = verb;
  
    const opcodes = {
      "1": (a, b, c) => {
        data[c] = data[a] + data[b];
      },
      "2": (a, b, c) => {
        data[c] = data[a] * data[b];
      }
    };

    const computer = Computer({data, opcodes});
    return computer.execute(debug);
  }

  function two() {
    for (let noun = 0; noun < 100; noun += 1) {
      for (let verb = 0; verb < 100; verb += 1) {
        console.log({noun, verb});
        const result = one(noun, verb, false);
        if (result === 19690720) {
          return (100 * noun) + verb;
        }
      }
    }
  }

  return Object.freeze({one, two});
}
