const fs = require("fs");

function parse(num, splitPattern = "\n") {
  return fs
    .readFileSync(`./data/${num}.txt`, "utf8")
    .trim()
    .split(splitPattern);
}

function sum(arr) {
  return arr.reduce((acc, cur) => {
    return acc + cur;
  }, 0);
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function permutations(array) {
  let result = [];
  const permute = (arr, m = []) => {
    if (arr.length === 0) {
      result.push(m);
    } else {
      for (let i = 0; i < arr.length; i++) {
        let cur = arr.slice();
        let next = cur.splice(i, 1);
        permute(cur.slice(), m.concat(next));
      }
    }
  };
  permute(array);
  return result;
}

module.exports = { parse, sum, sleep, permutations };
