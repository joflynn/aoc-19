const { parse } = require("./utils");

module.exports = () => {

  const input = parse(4, "-").map(x => parseInt(x));

  function sixDigits(code) {
    return code > 99999 && code < 1000000;
  }

  function hasAdjacentPair(code) {
    const digits = code.toString().split("");
    let prev;
    let found = false;
    digits.forEach(cur => {
      if (prev && prev === cur) {
        found = true;
      }
      prev = cur;
    });
    return found;
  }

  function hasAdjacentPairNotTrip(code) { 
    const digits = code.toString().split("").map(x => parseInt(x));
    const counts = [];
    for(let i = 0; i <= 9; i += 1) {
      let count = 0;
      digits.forEach(cur => {
        if (cur === i) {
          count += 1;
        }
      });
      counts[i] = count;
    }
    return counts.some(x => x === 2);

    let prev
    let prevPrev;
    let prevPrevPrev;
    let found = false;
    digits.forEach(cur => {

      if (prev && prevPrev) {
        if (prev === prevPrev && prevPrev !== cur && (prevPrevPrev === undefined || prevPrevPrev !== prev)) {
          found = true;
        }
      }
      //console.log({prevPrevPrev, prevPrev, prev, cur, found});
      prevPrevPrev = prevPrev;
      prevPrev = prev;
      prev = cur;
    });
/*    if (!found && prev === prevPrev) {
      found = true;
    }*/
    return found;
  }

  function monotonic(code) {
    const digits = code.toString().split("");
    let prev;
    let valid = true;
    digits.forEach(cur => {
      if (prev && prev > cur) {
        valid = false;
      }
      prev = cur;
    });
    return valid;
  }

  function one() {

    function valid(code) {
      return sixDigits(code)
        && hasAdjacentPair(code)
        && monotonic(code);
    }

    const test = [111111, 223450, 123789];
    test.forEach(t => console.log(t, valid(t)));

    let matches = 0;
    console.log(input);
    for (let i = input[0]; i < input[1]; i += 1) {
      if (valid(i)) {
        matches += 1;
      }
    }
    return matches;
  }

  function two() {
    function valid(code) {
      return sixDigits(code)
        && hasAdjacentPairNotTrip(code)
        && monotonic(code);
    }

    const test = [112233, 123444, 111122, 112222, 123345, 111234, 111224, 123334];
    test.forEach(t => console.log(t, valid(t)));
    //return;
    let matches = 0;
    //console.log(input);
    for (let i = input[0]; i < input[1]; i += 1) {
      if (valid(i)) {
        matches += 1;
      }
    }
    return matches;
  }

  return Object.freeze({one, two});
}
