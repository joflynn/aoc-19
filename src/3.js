const { parse, sum } = require("./utils");


module.exports = () => {

  const input = parse(3).map(x => x.split(","));
  const test = [
    ["R75","D30","R83","U83","L12","D49","R71","U7","L72"],
    ["U62","R66","U55","R34","D71","R55","D58","R83"]
  ];

  function process(visited, command) {
    const chars = command.split("");
    const dir = chars.shift();
    const dist = parseInt(chars.join(""));

    console.log(dir, dist);
    let { x, y } = visited[visited.length - 1];

    for (let i = 0; i < dist; i += 1) {
      switch (dir) {
        case "U":
          y += 1;
          break;
        case "R":
          x += 1;
          break;
        case "D":
          y -= 1;
          break;
        case "L":
          x -= 1;
          break;
      }
      visited.push({x, y});
    }

  }

  function build() {
    const [wireOne, wireTwo] = input;

    wireOneVisited = [{x: 0, y: 0}];
    wireOne.forEach(command => {
      process(wireOneVisited, command);
    });
    wireTwoVisited = [{x: 0, y: 0}];
    wireTwo.forEach(command => {
      process(wireTwoVisited, command);
    });
    return { wireOneVisited, wireTwoVisited };
  }

  function one() {
    const { wireOneVisited, wireTwoVisited } = build();
    
    const collisions = [];
    let minDistance = 999999;
    wireTwoVisited.forEach(({x, y}) => {
      if (wireOneVisited.find(pos => pos.x === x && pos.y === y)) {
        collisions.push({x, y});
        const dist = Math.abs(x) + Math.abs(y);
        if (dist > 0 && dist < minDistance) { 
          console.log({x, y});
          minDistance = dist;
        }
      }
    });

    return minDistance;
  }

  function two() {
    const { wireOneVisited, wireTwoVisited } = build();

    const collisions = [];
    let minDistance = 9999999;
    wireTwoVisited.forEach(({x, y}, idx2) => {
      const idx = wireOneVisited.findIndex(pos => pos.x === x && pos.y === y );
      if (idx > -1) {
        collisions.push({x, y});
        const dist = idx + idx2;
        if ( dist > 0 && dist < minDistance) {
          console.log({x, y});
          minDistance = dist;
        }
      }
    });
    return minDistance;
  }

  return Object.freeze({one, two});
}
